Vincity Ocean Park quy hoạch chuẩn Singapore và hơn thế nữa...Lấy cảm hứng từ mô hình Foster City ở Singapore với khu đô thị chuẩn kiểu mẫu đa chức năng vơi hệ thống biển hồ và xa trung tâm để giúp cư dân có được không khí thư giãn tránh ồn ào mỗi khi đi làm về và Vincity Ocean Park - Gia Lâm là mô hình đầu tiên của Vingroup trển khai tại Việt Nam.
Vincity Ocean Park nơi an cư lý tưởng cho cuộc sống thăng hoa, cộng đồng cư dân đẳng cấp.
1. Tổng quan Vincity Ocean Park
Tên dự án: VinCity Ocean Park
Quy mô dự án: Khoảng 420 ha
Chủ đầu tư: Tập đoàn VinGroup (tập đoàn kinh tế tư nhân lớn nhất Việt Nam)
Vị trí dự án: Phía đông bắc thủ đô, Khu đất vàng – Nằm giữa quốc lộ 5A và 5B. Nằm trên địa phận xã Đa Tốn – Kiêu Kỵ – Dương Xá và một phần thị trấn Trâu Qùy, Huyện Gia Lâm, TP Hà Nội.
Loại hình phát triển : Căn hộ chung cư, biệt thự, nhà vườn,nhà liền kề, nhà thương mại dịch vụ (shophouse)
Khu cao tầng gồm 4 phân khu:
1. The Park : Lấy nguồn cảm hứng từ công viên Central Park New York nổi tiếng nước Mỹ, The Park Vincity Ocean Park mang bóng dáng của sự hoàn hảo về một kđt văn minh, năng động sự kết hợp của các tòa chung cư hiện đại cùng với hệ thống công viên cây xanh hồ điều hòa.
2. The River : Nằm ở phía tây của khu đô thị, điểm nhấn là những dòng sông nước sinh thái uốn lượn.
3. The Sea : Là phân khu được đánh giá là độc đáo bậc nhất với điểm nhất là hồ nước mặn
4. The Lake : Sự hiện hữu của các căn biệt thự,nhà liền kề đẳng cấp nằm dọc theo mặt hồ trung tâm, phân khu The Lake Vincity Ocean Park sẽ là biểu tượng cho sự thịnh vượng của Vincity Ocean Park
Phân khu The Park sẽ được phát triển đầu tiên. Tiêu chuẩn bàn giao căn hộ đa dạng phù hợp với nhu cầu sử dụng và tầm tài chính của từng đối tượng khách hàng (bán hoàn thiện liền tường, hoàn thiện liền tường, hoàn thiện liền tường và đồ rời)
Khu thấp tầng : Vinhomes Ocean Park
Dự kiến : bắt đầu từ 2020 đi vào bàn giao và hoạt động
Ngoài ra dự án còn sở hữu 62 ha công viên và cây xanh | Mật độ tb xây dựng lý tưởng 19.2%
Loại hình sản phẩm: 66 tòa chung cư và khoảng hơn 2300 căn biệt thự, liền kề, nhà phố thương mại, trung tâm thương mại và các tiện ích dịch vụ khác.
Đặc biệt tại Vincity Ocean Park còn sở hữu biển hồ nước mặn: 6.1ha và Hồ điều hòa : 24.5ha

 Nguồn : http://www.muanhadep.vn/vincity-ocean-park-gia-lam-7065.html